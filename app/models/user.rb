class User < ApplicationRecord

	has_many :comments

	validates :password, format: { 
                       with: VALID_PASSWORD_REGEX, 
                       message: 'should contain uppercase, lowercase, number and special character. Between 6 and 16 characters long' 
                     }, 
                     allow_nil: true

end
